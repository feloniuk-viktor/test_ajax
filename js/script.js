let photos = []

function getPhotos() {
    return fetch('https://jsonplaceholder.typicode.com/photos?_limit=150')
        .then(res => res.json())
        .then(resPhotos => resPhotos)
}

function renderPhotos(photos) {
    const ul = document.querySelector('.list')
    ul.innerHTML = ''

    for (const photo of photos) {
        const li = document.createElement('li')
        li.classList.add('list-group-item')
        li.innerText = photo.id + ' ' + photo.title
        ul.append(li)
    }
}

getPhotos()
    .then(p => {
        photos = p
        renderPhotos(p)
    })

const input = document.querySelector('#exampleInputEmail1')

input.addEventListener('input', function (event) {
    const filterText = event.target.value

    if (filterText.length < 3) return

    renderPhotos(photos.filter(p => p.title.includes(filterText)))
})



